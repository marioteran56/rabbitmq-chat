# Arquitectura de Chat con RabbitMQ

## Descripción

Este proyecto consiste en un chat entre dos usuarios utilizando **RabbitMQ** como broker de mensajes. El proyecto está desarrollado en **JS** utilizando la libreria de **amqplib** para la conexión con el broker.

### Colas de mensajes

Para crear una nueva cola de mensajes entre dos usuarios, primeramente se creo una cola llamada **users-waiting** para agregar usuarios en espera de otro usuario para hablar por medio de mensajes. Cuando un usuario ingresa su nombre, este es agregado a la cola de espera, y cuando otro usuario ingresa su nombre, este es sacado de la cola de espera y se crea una nueva cola de mensajes con el nombre de los dos usuarios que se van a comunicar (ej. **fulano-fulana**).

#### Mensajes

Los mensajes son enviados a la cola con un identificador de correlación, el cual contiene el nombre del usuario que envía el mensaje. Cuando un usuario recibe el mensaje, revisa si el identificador de correlación es igual a su nombre, si es así, el mensaje es devuelto a la cola para que otro usuario lo reciba, si no, el mensaje es mostrado en pantalla con el nombre de usuario que lo envió.

## Requerimientos

Para poder ejecutar el proyecto, se necesita tener instalado **NodeJS** y contar con un broker de **RabbitMQ**, ya sea en un contenedor de **Docker** o en un servidor en la nube com **AWS** o **GCP**.

Una vez teniendo el broker de **RabbitMQ** se debe cambiar la variable **uri** en el archivo **chat.js** con la dirección del broker.

```js
const uri = "<uri_de_conexión_del_broker>";
```

## Instalación

Para instalar el proyecto, se debe clonar el repositorio y ejecutar el siguiente comando en la carpeta raíz del proyecto:

```bash
npm install
```

## Ejecución

Para ejecutar el proyecto, se debe ejecutar el siguiente comando en la carpeta raíz del proyecto:

```bash
node chat.js
```

Una vez ejecutado el comando, se debe ingresar el nombre de usuario y presionar enter, luego, dentro de otra terminal, se debe ejecutar el comando nuevamente y ingresar el nombre de usuario de la otra terminal, luego de esto, se conectarán ambos usuarios y se podrá enviar mensajes entre ellos.


![Chat](./chat-example.png)