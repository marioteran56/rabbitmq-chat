const amqp = require("amqplib/callback_api");
const uri = "amqp://mario:abcdef123456@localhost:5671";
const readline = require("readline");

// Función para añadir un usuario a la cola de usuarios y esperar a que otro usuario se conecte
function waitForConnection() {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  amqp.connect(uri, (err, conn) => {
    if (err) throw err;

    conn.createChannel((err, ch) => {
      if (err) throw err;

      const queue = "users-waiting";
      let user1, user2;
      ch.assertQueue(queue, { durable: false, messageTtl: 60000 });

      // Agregamos el nuevo usuario a la cola
      rl.question("Introduce tu nombre de usuario: ", (answer) => {
        ch.sendToQueue(queue, Buffer.from(answer));
        user1 = answer;

        // Esperamos a que un usuario se conecte y nos conectamos al chat
        console.log("\nEsperando a otro usuario...");
        ch.consume(
          queue,
          (msg) => {
            if (msg.content.toString() === user1) {
              ch.nack(msg, false, true);
            } else {
              user2 = msg.content.toString();
              ch.ack(msg);
              // Nos conectamos al chat
              console.log(`\nConectando con ${user2}...\n`);
              ch.close();
              connectToChat(conn, user1, user2);
            }
          },
          { noAck: false }
        );

        rl.close();
      });
    });
  });
}

function connectToChat(conn, user1, user2) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  conn.createChannel((err, ch) => {
    if (err) throw err;

    // Reacomodamos los usuarios para que el orden sea siempre el mismo
    users = [user1, user2];
    users.sort();

    const queue = `${users[0]}-${users[1]}`;
    ch.assertQueue(queue, { durable: false });

    // Recibimos los mensajes del chat
    ch.consume(
      queue,
      (msg) => {
        if (msg.properties.correlationId === user1) {
          ch.nack(msg, false, true);
        } else {
          console.log(`${user2}: ${msg.content.toString()}`);
          ch.ack(msg);
        }
      },
      { noAck: false }
    );

    // Enviamos los mensajes al chat
    rl.on("line", (input) => {
      ch.sendToQueue(queue, Buffer.from(input), { correlationId: user1 });
      readline.moveCursor(rl.output, 0, -1);
      readline.clearLine(rl.output, 0);
      console.log(`Tu: ${input}`);
    });
  });
}

waitForConnection();
